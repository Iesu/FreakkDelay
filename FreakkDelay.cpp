	#include "FreakkDelay.h"
#include "IPlug_include_in_plug_src.h"

#include <math.h>
#include "../../WDL/denormal.h"
#define SMOOTH_MS 50 // milliseconds of crossfade between old and new delay


enum modParams 
{
	MOD_BYPASS = 0,
	MOD_ON,
};

FreakkDelay::FreakkDelay(IPlugInstanceInfo instanceInfo):
  IPLUG_CTOR(kNumParams, 0, instanceInfo)
{
  TRACE;
  samplingrate = GetSampleRate();
  /*
  DlyBufferSize = (int) (MAX_DLY_TIME * samplingrate);
  DlyBuffer1 = new double[DlyBufferSize]();
  DlyBuffer2 = new double[DlyBufferSize]();
  */
  smoothCounter = 0;
  smoothSamples = SMOOTH_MS * samplingrate/1000;
  int default_mTime = 300; // milliseconds
  counter=0;
 
  //arguments are: name, defaultVal, minVal, maxVal, step, label
  GetParam(kGain)->InitDouble("Gain", 1, 0., 1.5, 0.001, ""); // Gain knob offers a little amplification
  GetParam(kTime)->InitDouble("Time", default_mTime, 0, MAX_DLY_TIME*1000, 10, "ms");
  GetParam(kFeedback)->InitDouble("Feedback", 50, 0, 90,1, "%");
  GetParam(kMix)->InitDouble("Level", 50, 0, 100,1, "%");
  
  DelayL = new DelayLine(samplingrate, MAX_DLY_TIME, mTime);
  DelayR = new DelayLine(samplingrate, MAX_DLY_TIME, mTime);
  DelayedSampleL = DelayedSampleL = 0;

  //Generate Background
  IGraphics* pGraphics = MakeGraphics(this, kW, kH); // MakeGraphics(this, kW, kH);
  pGraphics->AttachBackground(BG_ID, BG_FN);
  
  //Generate Knobs
  IBitmap bitmap = pGraphics->LoadIBitmap(KNOB_TIME_ID, KNOB_TIME_FN);
  pGraphics->AttachControl(new IKnobRotaterControl(this, kTimeKnob_def_X, kTimeKnob_def_Y, kTime, &bitmap));
  
  bitmap = pGraphics->LoadIBitmap(KNOB_FEEDBACK_ID, KNOB_FEEDBACK_FN);
  pGraphics->AttachControl(new IKnobRotaterControl(this, kFeedbackKnob_def_X, kFeedbackKnob_def_Y, kFeedback, &bitmap));
  
  bitmap = pGraphics->LoadIBitmap(KNOB_MIX_ID, KNOB_MIX_FN);
  pGraphics->AttachControl(new IKnobRotaterControl(this, kMixKnob_def_X, kMixKnob_def_Y, kMix, &bitmap));
  AttachGraphics(pGraphics);
  
}


void FreakkDelay::OnParamChange(int paramIdx)
{
  IMutexLock lock(this);

  switch (paramIdx)
  {
    case kGain:
	  mGain = GetParam(kGain)->Value();
	  break;
	case kTime:
	  mTime = GetParam(kTime)->Value();
	  DelayL->SetDlyTime(mTime);
	  DelayR->SetDlyTime(mTime);
	  break;
	case kFeedback:
	  mFeedback = GetParam(kFeedback)->Value();
	  break;
	case kMix:
	  mMix = GetParam(kMix)->Value()/100.;
	  break;
  }
}


void FreakkDelay::ProcessDoubleReplacing(double** inputs, double** outputs, int sampleFrames)
{
	double* in1 = inputs[0];
	double* in2 = inputs[1];
	double* out1 = outputs[0];
	double* out2 = outputs[1];

	bool isMono = !IsInChannelConnected(1);
	
	for (int counter = 0; counter < sampleFrames; ++counter, ++in1, ++in2, ++out1, ++out2)
	{
		
	    //DlyBuffer1[write_position] = *in1 + DlyBuffer1[read_position]*mFeedback/100;
		//DlyBuffer2[write_position] = *in2 + DlyBuffer2[read_position]*mFeedback/100;
		DelayedSampleL = DelayL->Read();
		DelayedSampleR = DelayR->Read();
		DelayL->Step();
		DelayR->Step();	

		DelayL->Write( *in1 + DelayedSampleL * mFeedback/100 );
		DelayR->Write( *in2 + DelayedSampleR * mFeedback/100 );

		*out1 = ( *in1 *(1-mMix) + DelayedSampleL * mMix ) * mGain;
		*out2 = ( *in2 *(1-mMix) + DelayedSampleR * mMix ) * mGain;
	}

}


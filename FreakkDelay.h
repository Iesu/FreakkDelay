#ifndef __FREAKKDELAY_H__
#define __FREAKKDELAY_H__
#define MAX_DLY_TIME 3 //max delay time in seconds
/*

 IPlug distortion example
 (c) Theo Niessink 2011
 <http://www.taletn.com/>

 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.

 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:

 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software in a
 product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.


 Simple IPlug audio effect that shows how to implement oversampling to reduce
 aliasing.

 */


#include "IPlug_include_in_plug_hdr.h"

#define WDL_BESSEL_FILTER_ORDER 8
#define WDL_BESSEL_DENORMAL_AGGRESSIVE
#include "../../WDL/besselfilter.h"
#include "OnePole.h"
#include "../FrkDSP/DelayLine.h"

enum EParams
{
  kGain= 0,
  kTime,
  kFeedback,
  kMix,
  kNumParams
};

enum ELayout
{
  kW = GUI_WIDTH,  // width of plugin window
  kH = GUI_HEIGHT,	// height of plugin window

  kTimeKnob_def_X = 95,
  kTimeKnob_def_Y = 40,

  kFeedbackKnob_def_X = 28,
  kFeedbackKnob_def_Y = 102,

  kMixKnob_def_X = 166,
  kMixKnob_def_Y = 102
};

class FreakkDelay: public IPlug
{
public:
  FreakkDelay(IPlugInstanceInfo instanceInfo);
  ~FreakkDelay(){

  }

  void OnParamChange(int paramIdx);
  void ProcessDoubleReplacing(double** inputs, double** outputs, int nFrames);

private:
  double mGain, mTime, frac, mFeedback, mMix;
  double DelayedSampleL, DelayedSampleR ;
  long counter;
  int MaxDlyTime;
  int DlyBufferSize;
  int read_position, write_position;
  double samplingrate;
  int smoothSamples, smoothCounter;
  DelayLine * DelayL;
  DelayLine * DelayR;
};


#endif // __FREAKKDELAY_H__
